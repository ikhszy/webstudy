<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DateAppointmentDate</name>
   <tag></tag>
   <elementGuidId>dabc2ea6-7b54-46f4-8ce2-84e8996d2b77</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@date = '&lt;td class=&quot;day&quot;>24&lt;/td>']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>date</name>
      <type>Main</type>
      <value>&lt;td class=&quot;day&quot;>24&lt;/td></value>
   </webElementProperties>
</WebElementEntity>
